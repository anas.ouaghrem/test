package com.vente.springjwt.controllers;


import com.vente.springjwt.repository.CategoryRepository;
import com.vente.springjwt.repository.SubcategoryRepository;
import com.vente.springjwt.models.Subcategory;
import com.vente.springjwt.services.SubcategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/subcategory")
@CrossOrigin("*")
public class SubcategoryController {
    @Autowired
    SubcategoryService subcategoryService;
    
    @Autowired
    private SubcategoryRepository subcategoryRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping
    public List<Subcategory> getall() {
        return subcategoryService.getall();
    }

    @PostMapping
    public Subcategory createSubcategory(@RequestBody Subcategory s) {
        return subcategoryService.createSubcategoryService(s);
    }
//    @PostMapping("/{id_categorie}")
//    public Subcategory creatSubcategory(@RequestBody Subcategory s, @PathVariable Long id_categorie) {
//        return subcategoryService.createSubcategoryService(s, id_categorie);
//    }
//    @GetMapping("/Subcategory/{id_categ}")
//
//    public ResponseEntity<List<Subcategory>> listCateg(@PathVariable Long id_categorie) {
//
//        Optional<Subcategory> Subcategorys = subcategoryRepository.findById(id_categorie);
//
//        return new ResponseEntity<List<Subcategory>>(Subcategory, HttpStatus.OK);
//    }




    @GetMapping("/getone/{id}")
    public Subcategory getOnesubcategory(@PathVariable Long id) {
        return subcategoryRepository.findById(id).orElse(null);
    }

    @PutMapping("/{id}")
    public Subcategory updateSubcategory(@RequestBody Subcategory s, @PathVariable Long id) {
        return subcategoryService.updateSubcategoryService(s, id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteSubcategory(@PathVariable Long id) {
        return subcategoryService.deleteSubcategoryService(id);
    }







}