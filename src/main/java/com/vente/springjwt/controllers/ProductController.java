package com.vente.springjwt.controllers;

import com.vente.springjwt.repository.ProductRepository;
import com.vente.springjwt.repository.ProviderRepository;
import com.vente.springjwt.repository.SubcategoryRepository;
import com.vente.springjwt.models.Product;
import com.vente.springjwt.models.Provider;
import com.vente.springjwt.models.ResponseMessage;
import com.vente.springjwt.models.Subcategory;
import com.vente.springjwt.services.ProductService;
import com.vente.springjwt.utils.StorageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@RequestMapping("/product")
@CrossOrigin("*")
public class ProductController {
    @Autowired
    ProductService productService;

    @Autowired
    private ProductRepository productrepository;

    @Autowired
    private SubcategoryRepository subcategoryRepository;

    @Autowired
    private ProviderRepository providerRepository;

    @Autowired
    private StorageService storage;

    private final Path rootLocation = Paths.get("upload-dir");


    @Operation(summary = "Find All Product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = Product.class))),
            @ApiResponse(responseCode = "404", description = "Product not found", content = @Content) })

    @GetMapping("/all")
    @PreAuthorize("hasRole('ADMIN') or hasRole('CUSTOMER') ")
    public List<Product> getallProduct() {
        return productrepository.findAll();
    }

    @Operation(summary = "Get a product by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product Found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Product.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "product not found",
                    content = @Content) })

    @GetMapping("/getone/{id}")
    public Product getOneProduct(@Parameter(description = "id of product to be searched") @PathVariable Long id) {
        return productService.getOneProductService(id);
    }

    @GetMapping
    public List<Product> getall(){
        return productService.getall() ;
    }

    @PostMapping
    public Product createProduct (@RequestBody Product p){
        return productService.createProductService(p);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteProduct (@PathVariable Long id) {
        return productService.deleteproductService(id);
    }

    @PutMapping("/{id}")
    public Product updateProduct (@RequestBody Product p, @PathVariable Long id){
        return productService.updateproductService(p,id);
    }
    @PostMapping("/upload/{id_fournisseur}/{id_souscategorie}")
    public ResponseEntity<ResponseMessage> uploadfilesService(@RequestParam("files") MultipartFile[] files, Product p,
                                                              @PathVariable Long id_souscategorie, @PathVariable Long id_fournisseur ) {
        return productService.uploadfilesService(files,p,id_souscategorie,id_fournisseur);
    }

    @PostMapping("/{id_fournisseur}/{id_souscategorie}")
    public Product saveproduct(@RequestParam("file") MultipartFile file, Product product,
                               @PathVariable Long id_souscategorie, @PathVariable Long id_fournisseur ) {
        return productService.saveproductService(file,product,id_souscategorie,id_fournisseur);
    }


    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<Resource>getFileService(@PathVariable String filename){
        return productService.getFileService(filename);}


    @GetMapping("/Product")
    public ResponseEntity<List<String>> getProduct() {
        List<String> Product = Arrays.asList("Option 1", "Option 2", "Option 3");
        return ResponseEntity.ok(Product);
    }

    //les méthodes save avec les relations
    @PostMapping("/save/{idprovider}/{idsubcategory}")
    public Product saveproduct(@RequestBody Product p, @PathVariable Long idprovider, @PathVariable Long idsubcategory) {
        Provider provider= providerRepository.findById(idprovider).orElse(null);
        p.setProvider(provider);
        Subcategory sub= subcategoryRepository.findById(idsubcategory).orElse(null);
        p.setSubcategory(sub);
        return productrepository.save(p);
    }

  //update avec les relations
  @PutMapping("/update1/{Id}")
    public Product updateProduct1(@RequestBody Product p, @PathVariable Long Id) {
    Product p1 = productrepository.findById(Id).orElse(null);

    if (p1 != null) {
        p.setId(Id);

        p.setSubcategory(p1.getSubcategory());
        p.setProvider(p1.getProvider());

        return productrepository.saveAndFlush(p);
    }
    else{
        throw new RuntimeException("FAIL!");
    }

}








//pagination & pipe

    @GetMapping("/chercher")
    public List<Product> findProductbyName(String name) {

        return productrepository.chercher("%"+name+"%");
    }




    @GetMapping("/getbypage")
    public Page<Product> getproducts(int page,int size) {

        return productrepository.findbypage( PageRequest.of(page, size));
    }

    //add product with image


    @RequestMapping(
            path = "/save2",
            method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Product saveproduct(@RequestParam(value = "file") MultipartFile file, Product product){
        storage.store(file);
        product.setImage(file.getOriginalFilename());
        return productrepository.save(product);
    }




    @RequestMapping(
            path = "/save1/{idprovider}/{idsubcategory}",
            method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Product saveproduct2(@RequestParam("file") MultipartFile file, Product p,@PathVariable Long idprovider, @PathVariable Long idsubcategory ) {
        try {
            String fileName = Integer.toString(new Random().nextInt(1000000000));
            String ext = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf('.'), file.getOriginalFilename().length());
            String name = file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf('.'));
            String original = name + fileName + ext;
            Files.copy(file.getInputStream(), this.rootLocation.resolve(original));
            p.setImage(original);

            Provider provider= providerRepository.findById(idprovider).orElse(null);
            p.setProvider(provider);
            Subcategory sub= subcategoryRepository.findById(idsubcategory).orElse(null);
            p.setSubcategory(sub);
        } catch (Exception e) {
            throw new RuntimeException("FAIL File Prolem BackEnd !");
        }

        return productrepository.save(p);
    }









// add product with gallery d'images







    @PutMapping("/updateqte/{Id}")
    public Product updateProductqte(String qte, @PathVariable Long id) {
        return productService.updateProductqteService(qte,id);
    }








































}
