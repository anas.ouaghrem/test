package com.vente.springjwt.controllers;


import com.vente.springjwt.models.ResponseMessage;
import com.vente.springjwt.models.Vehicule;
import com.vente.springjwt.services.VehiculeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/Vehicules")
@CrossOrigin("*")
public class VehiculeController {

    @Autowired
    VehiculeService vehiculeService ;
    @GetMapping
    public List<Vehicule> getall(){
        return vehiculeService.getall() ;
    }

    @GetMapping("/{id}")
    public Vehicule getOneVehicule(@PathVariable Long id){
        return vehiculeService.getOneVehiculeService(id);
    }


    @PostMapping
    public Vehicule createVehicule (@RequestBody Vehicule v){
        return vehiculeService.createVehiculeService(v);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteVehicule (@PathVariable Long id) {
        return vehiculeService.deleteVehiculeService(id);
    }



    @PutMapping("/{id}")
    public Vehicule updateVehicule (@RequestBody Vehicule v, @PathVariable Long id){
        return vehiculeService.updateVehiculeService(v,id);
    }
    @PostMapping("/upload/{id_fournisseur}/{id_souscategorie}")
    public ResponseEntity<ResponseMessage> uploadfilesService(@RequestParam("files") MultipartFile[] files, Vehicule v,
                                                              @PathVariable Long id_souscategorie, @PathVariable Long id_fournisseur ) {
        return vehiculeService.uploadfilesService(files,v,id_souscategorie,id_fournisseur);
    }

    @PostMapping("/{id_fournisseur}/{id_souscategorie}")
    public Vehicule saveVehicule(@RequestParam("file") MultipartFile file, Vehicule vehicule,
                               @PathVariable Long id_souscategorie, @PathVariable Long id_fournisseur ) {
        return vehiculeService.saveVehiculeService(file,vehicule,id_souscategorie,id_fournisseur);
    }


    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<Resource>getFileService(@PathVariable String filename){
        return vehiculeService.getFileService(filename);}
}



