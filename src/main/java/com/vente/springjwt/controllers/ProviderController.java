package com.vente.springjwt.controllers;


import com.vente.springjwt.models.ERole;

import com.vente.springjwt.models.Provider;
import com.vente.springjwt.models.Role;
import com.vente.springjwt.models.User;
import com.vente.springjwt.payload.request.SignupRequest;
import com.vente.springjwt.payload.response.MessageResponse;
import com.vente.springjwt.repository.ProviderRepository;
import com.vente.springjwt.repository.RoleRepository;
import com.vente.springjwt.repository.UserRepository;
import com.vente.springjwt.services.ProviderService;
import com.vente.springjwt.services.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin("*")
@RequestMapping("/provider")
public class ProviderController {

    @Autowired
    ProviderService providerService;
    @Autowired
    UserService userService;
    @Autowired
    private ProviderRepository providerRepository;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder encoder;

    @GetMapping
    public List<Provider> getall(){
        return providerService.getall() ;    }

    @GetMapping("/{id}")
    public Provider getOneProvider(@PathVariable Long id){
        return providerService.getOneProviderService(id);
    }

/*    @PostMapping
    public Provider createProvider (@RequestBody Provider f){
        return providerService.createProviderService(f);
    }*/
    @DeleteMapping("/{id}")
    public ResponseEntity deleteProvider (@PathVariable Long id) {
        return providerService.deleteProviderService(id);
    }
    @PutMapping("/{id}")
    public Provider updateProvider (@RequestBody Provider f, @PathVariable Long id) {

        return providerService.updateProviderService(f, id);
    }


        @GetMapping("/files/{filename:.+}")
        public ResponseEntity<Resource>getFileService(@PathVariable String filename){
            return userService.getFileService(filename);}





    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signupRequest) {
        if (userRepository.existsByUsername(signupRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signupRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        Provider provider = new Provider(signupRequest.getUsername(),
                signupRequest.getEmail(),
                encoder.encode(signupRequest.getPassword()),signupRequest.getMatricule(),signupRequest.getService(),signupRequest.getCompany());


        Set<String> strRoles = signupRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            Role provRole = roleRepository.findByName(ERole.ROLE_PROVIDER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(provRole);


        }
        provider.setConfirm(true);
        provider.setRoles(roles);
        providerRepository.save(provider);

        return ResponseEntity.ok(new MessageResponse("provider registered successfully!"));
    }

}
