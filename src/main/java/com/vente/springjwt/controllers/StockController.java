package com.vente.springjwt.controllers;


import com.vente.springjwt.models.Stock;
import com.vente.springjwt.services.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Stocks")
@CrossOrigin("*")
public class StockController {

    @Autowired
    StockService stockService;


    @GetMapping
    public List<Stock> getall(){
        return stockService.getall() ;
    }

    @GetMapping("/{id}")
    public Stock getOneStock(@PathVariable Long id){
        return stockService.getOneStockService(id);
    }

    @PostMapping
    public Stock createStock (@RequestBody Stock s){
        return stockService.createStockService(s);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteStock (@PathVariable Long id) {
        return  stockService.deleteStockService(id);

    }
    @PutMapping("/{id}")
    public Stock updateStock (@RequestBody Stock s, @PathVariable Long id){
        return stockService.updateStockService(s,id);
    }
}
