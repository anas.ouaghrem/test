package com.vente.springjwt.controllers;


import com.vente.springjwt.models.Product;
import com.vente.springjwt.models.ResponseMessage;
import com.vente.springjwt.models.User;
import com.vente.springjwt.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/User")
@CrossOrigin("*")
public class UserController {
    @Autowired
    UserService userService;


    @GetMapping
    public List<User> getall(){
        return userService.getall() ;
    }

    @GetMapping("/{id}")
    public User getOneUser(@PathVariable Long id){
        return userService.getOneUserService(id);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser (@PathVariable Long id) {
        userService.deleteUserService(id);
        return ResponseEntity.ok().build();
    }
    @PutMapping("/{id}")
    public User updateUser (@RequestBody User u, @PathVariable Long id){
            return userService.updateUserService(u,id);
    }

//    @PostMapping
//    public User saveuser(@RequestParam("file") MultipartFile file, User user ) {
//        return userService.saveuserService(file,user);
//    }

    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> uploadfilesService(@RequestParam("files") MultipartFile[] files, User u) {
        return userService.uploadfilesService(files,u);
    }

    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<Resource>getFileService(@PathVariable String filename){
        return userService.getFileService(filename);}



}
