package com.vente.springjwt.controllers;



import com.vente.springjwt.models.Immobilier;
import com.vente.springjwt.models.ResponseMessage;
import com.vente.springjwt.services.ImmobilierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/Immobiliers")
@CrossOrigin("*")
public class ImmobilierController {
    @Autowired
    ImmobilierService immobilierService;


    @GetMapping
    public List<Immobilier> getall(){
        return immobilierService.getall() ;
    }

    @GetMapping("/{id}")
    public Immobilier getOneImmobilier(@PathVariable Long id){
        return immobilierService.getOneImmobilierService(id);
    }


    @PostMapping
    public Immobilier createImmobilier (@RequestBody Immobilier i){
        return immobilierService.createImmobilierService(i);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteImmobilier (@PathVariable Long id) {
        return immobilierService.deleteImmobilierService(id);
    }



    @PutMapping("/{id}")
    public Immobilier updateImmobilier (@RequestBody Immobilier i, @PathVariable Long id){
        return immobilierService.updateImmobilierService(i,id);
    }
    @PostMapping("/upload/{id_fournisseur}/{id_souscategorie}")
    public ResponseEntity<ResponseMessage> uploadfilesService(@RequestParam("files") MultipartFile[] files, Immobilier i,
                                                              @PathVariable Long id_souscategorie, @PathVariable Long id_fournisseur ) {
        return immobilierService.uploadfilesService(files,i,id_souscategorie,id_fournisseur);
    }

    @PostMapping("/{id_fournisseur}/{id_souscategorie}")
    public Immobilier saveImmobilier(@RequestParam("file") MultipartFile file, Immobilier immobilier,
                               @PathVariable Long id_souscategorie, @PathVariable Long id_fournisseur ) {
        return immobilierService.saveImmobilierService(file,immobilier,id_souscategorie,id_fournisseur);
    }


    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<Resource>getFileService(@PathVariable String filename){
        return immobilierService.getFileService(filename);}
}

