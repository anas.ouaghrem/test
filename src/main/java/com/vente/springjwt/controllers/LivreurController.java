package com.vente.springjwt.controllers;

import com.vente.springjwt.models.*;
import com.vente.springjwt.models.Livreur;
import com.vente.springjwt.payload.request.SignupRequest;
import com.vente.springjwt.payload.response.MessageResponse;
import com.vente.springjwt.repository.LivreurRepository;
import com.vente.springjwt.repository.RoleRepository;
import com.vente.springjwt.repository.UserRepository;
import com.vente.springjwt.services.LivreurService;
import com.vente.springjwt.services.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@RestController
@CrossOrigin("*")
@RequestMapping("/livreur")
public class LivreurController {

    @Autowired
    LivreurService livreurService;
    @Autowired
    UserService userService;
    @Autowired
     LivreurRepository livreurRepository;

    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder encoder;



    @GetMapping
    public List<Livreur> getall(){
        return livreurService.getall() ;    }
    
    @GetMapping("/{id}")
    public Livreur getOneLivreur(@PathVariable Long id){
        return livreurService.getOneLivreurService(id);
    }



    @DeleteMapping("/{id}")
    public ResponseEntity deleteLivreur (@PathVariable Long id) {
        return livreurService.deleteLivreurService(id);
    }
    @PutMapping("/{id}")
    public Livreur updateLivreur (@RequestBody Livreur l, @PathVariable Long id) {

        return livreurService.updateLivreurService(l, id);
    }



    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signupRequest) {
        if (userRepository.existsByUsername(signupRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signupRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        Livreur livreur = new Livreur(signupRequest.getUsername(),
                signupRequest.getEmail(),
                encoder.encode(signupRequest.getPassword()),signupRequest.getMatricule(),signupRequest.getService(),signupRequest.getCompany());


        Set<String> strRoles = signupRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            Role provRole = roleRepository.findByName(ERole.ROLE_LIVREUR)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(provRole);


        }
        livreur.setConfirm(true);
        livreur.setRoles(roles);
        livreurRepository.save(livreur);

        return ResponseEntity.ok(new MessageResponse("livreur registered successfully!"));
    }
}
