package com.vente.springjwt.controllers;

import com.vente.springjwt.repository.ClientRepository;
import com.vente.springjwt.repository.OrderRepository;
import com.vente.springjwt.repository.ProductRepository;
import com.vente.springjwt.models.Client;
import com.vente.springjwt.models.Order;
import com.vente.springjwt.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderRepository orderRepository;



    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ProductRepository productrepository;


    @GetMapping("/all")
    public List<Order> getAllorder() {
        return orderRepository.findAll();
    }

    @PostMapping("/save")
    public Order saveorder(@RequestBody Order o) {
        return orderRepository.save(o);
    }

    @GetMapping ("/getone/{id}")
    public Order getOneorder(@PathVariable Long id){
        return orderRepository.findById(id).orElse(null);
    }

    @PutMapping("/update/{Id}")
    public Order update(@RequestBody Order o, @PathVariable Long Id) {
        Order o1 = orderRepository.findById(Id).orElse(null);
        if (o1!= null) {
            o.setId(Id);
            return orderRepository.saveAndFlush(o);
        }
        else{
            throw new RuntimeException("FAIL!");
        }
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String,String> deleteClient(@PathVariable Long Id) {
        HashMap message= new HashMap();
        try{
            orderRepository.deleteById(Id);
            message.put("etat","order deleted");
            return message;
        }
        catch (Exception e) {
            message.put("etat","order not deleted");
            return message;
        }
    }

    //les méthodes save avec les relations

    @PostMapping("/save/{idclient}")
    public Order saveorder(@RequestBody Order o, @PathVariable Long idclient) {
        Client c= clientRepository.findById(idclient).orElse(null);
        o.setClient(c);
        return orderRepository.save(o);
    }


    @PostMapping("/save1/{idclient}")
    public Order saveorder(@RequestBody Order o, @RequestParam List<Long> ids,@PathVariable Long idclient){
        for(int i=0; i< ids.size(); i++) {
            Product p = productrepository.findById(ids.get(i)).orElse(null);
            o.addproduct(p);
            System.out.println( p );

        }
        Client c= clientRepository.findById(idclient).orElse(null);
        o.setClient(c);
        orderRepository.save(o);
        return orderRepository.save(o);
    }

    //update avec les relations

    @PutMapping("/update1/{Id}")
    public Order update1(@RequestBody Order o, @PathVariable Long Id) {
        Order o1 = orderRepository.findById(Id).orElse(null);
        if (o1!= null) {
            o.setId(Id);
            o.setClient(o1.getClient());
            return orderRepository.saveAndFlush(o);
        }
        else{
            throw new RuntimeException("FAIL!");
        }
    }







//getorberbyclient

    @GetMapping("/history")
    public ResponseEntity<List<Order>> getOrdersByClient( String name){
        Client client = clientRepository.findByClientname(name);
        if(client == null){
            return  ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(orderRepository.findByClient(client.getUsername()));
    }





}