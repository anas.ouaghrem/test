package com.vente.springjwt.controllers;


import com.vente.springjwt.models.Commande;
import com.vente.springjwt.services.CommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/Commande")
@CrossOrigin("*")
public class CommandeController {

    @Autowired
    CommandeService commandeService;


    @GetMapping
    public List<Commande> getall(){
        return commandeService.getall() ;
    }


    @PostMapping
    public Commande createCommande (@RequestBody Commande c){
        return commandeService.createCommandeService(c);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity deleteCommande (@PathVariable Long id) {
        return commandeService.deleteCommandeService(id);

    }
    @PutMapping("/{id}")
    public Commande updateCommande (@RequestBody Commande c, @PathVariable Long id){
            return commandeService.updateCommandeService(c,id);

    }


}
