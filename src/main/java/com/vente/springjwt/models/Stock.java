package com.vente.springjwt.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Collection;

@Entity
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String product;
    private String quantite;

    @OneToMany(mappedBy = "stock",cascade = CascadeType.REMOVE)
    private Collection<Product> Products;
    @JsonIgnore
    public Collection<Product> getProducts() {
        return Products;
    }

    public void setProducts(Collection<Product> products) {
        Products = products;
    }

    @OneToMany(mappedBy = "stock",cascade = CascadeType.REMOVE)
    private Collection<Vehicule> Vehicules;


    @OneToMany(mappedBy = "stock",cascade = CascadeType.REMOVE)
    private Collection<Immobilier> Immobiliers;

    @JsonIgnore
    public Collection<Immobilier> getImmobiliers() {
        return Immobiliers;
    }

    public void setImmobiliers(Collection<Immobilier> immobiliers) {
        Immobiliers = immobiliers;
    }

    @JsonIgnore
    public Collection<Vehicule> getVehicules() {
        return Vehicules;
    }

    public void setVehicules(Collection<Vehicule> vehicules) {
        Vehicules = vehicules;
    }

    public String getQuantite() {
        return quantite;
    }

    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }


}
