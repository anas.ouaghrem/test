package com.vente.springjwt.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Immobilier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String type;
    private String nb_chambre;
    private String nb_salle;
    private String price;
    private String adresse;
    private String surface;
    private String date;
    private String laverie;
    private String parking;
    private String climatisation;
    private String chauffage;
    private String descrtion;
    private String iamge;
    private ArrayList<String> images=new ArrayList<>();






    @ManyToOne
    @JoinColumn(name = "provider_id")
    private Provider provider;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    @ManyToOne
    @JoinColumn(name = "Subcategory_id")
    private Subcategory subcategory;

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }

    @ManyToOne
    @JoinColumn(name = "Stock_id")
    private Stock stock;

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @ManyToMany
    @JoinTable(name = "Commande_immobilier", joinColumns = @JoinColumn(name = "immobilier_id"),inverseJoinColumns = @JoinColumn(name ="commande_id"))
    private Collection<Commande> commandes;
    @JsonIgnore
    public Collection<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(Collection<Commande> commandes) {
        this.commandes = commandes;
    }

    @ManyToOne
    @JoinColumn(name = "Panier_id")
    private Panier panier;

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }







    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNb_chambre() {
        return nb_chambre;
    }

    public void setNb_chambre(String nb_chambre) {
        this.nb_chambre = nb_chambre;
    }

    public String getNb_salle() {
        return nb_salle;
    }

    public void setNb_salle(String nb_salle) {
        this.nb_salle = nb_salle;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLaverie() {
        return laverie;
    }

    public void setLaverie(String laverie) {
        this.laverie = laverie;
    }

    public String getParking() {
        return parking;
    }

    public void setParking(String parking) {
        this.parking = parking;
    }

    public String getClimatisation() {
        return climatisation;
    }

    public void setClimatisation(String climatisation) {
        this.climatisation = climatisation;
    }

    public String getChauffage() {
        return chauffage;
    }

    public void setChauffage(String chauffage) {
        this.chauffage = chauffage;
    }

    public String getDescrtion() {
        return descrtion;
    }

    public void setDescrtion(String descrtion) {
        this.descrtion = descrtion;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }


    public void setImage(String images) {
    }

    public String getIamge() {
        return iamge;
    }

    public void setIamge(String iamge) {
        this.iamge = iamge;
    }

    public void setImage(ArrayList<String> fileNames) {
    }
}
