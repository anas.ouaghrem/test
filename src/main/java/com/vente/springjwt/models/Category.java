package com.vente.springjwt.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Collection;


@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;



    @OneToMany (mappedBy = "category",cascade = CascadeType.REMOVE)
    private Collection<Subcategory> subcategorys;



    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }



    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @JsonIgnore
    public Collection<Subcategory> getSubcategorys() {
        return subcategorys;
    }

    public void setSubcategorys(Collection<Subcategory> subcategorys) {
        this.subcategorys = subcategorys;
    }
}
