package com.vente.springjwt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;

import java.util.Collection;

@Entity
public class Provider extends User{


    private String matricule;
    private String service;
    private String company;

    public Provider(String username, String email, String password, String matricule, String service, String company) {
        super(username, email, password);
        this.matricule = matricule;
        this.service = service;
        this.company = company;
    }

    public Provider() {
    }

    public Provider(String matricule, String service, String company, Collection<Product> products) {
        this.matricule = matricule;
        this.service = service;
        this.company = company;
        this.products = products;
    }

    public Provider(String username, String email, String password, String matricule, String service, String company, Collection<Product> products) {
        super(username, email, password);
        this.matricule = matricule;
        this.service = service;
        this.company = company;
        this.products = products;
    }

    @OneToMany(mappedBy = "provider",cascade = CascadeType.REMOVE)
    private Collection<Product> products;



    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getMatricule() {
        return matricule;
    }

    public String getService() {
        return service;
    }

    public String getCompany() {
        return company;
    }

    @JsonIgnore
    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }
}
