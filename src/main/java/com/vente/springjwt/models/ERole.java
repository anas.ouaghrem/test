package com.vente.springjwt.models;

public enum ERole {
  ROLE_USER,
  ROLE_PROVIDER,
  ROLE_CUSTOMER,
  ROLE_ADMIN,
  ROLE_LIVREUR
}
