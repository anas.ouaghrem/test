package com.vente.springjwt.models;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int price;
    private String date;
    private String qtetotal;






    @ManyToOne
    @JoinColumn(name="client_id")
    private Client client;


    @ManyToMany
    @JoinTable(name="PROD_Ord", joinColumns= @JoinColumn(name="ORD_ID"),
            inverseJoinColumns= @JoinColumn(name="PROD_ID"))
    private Collection<Product> products;




    public void setId(Long id) {
        this.id = id;
    }

    public void setPrice(int price) {
        this.price = price;
    }


    public Long getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPrice() {
        return price;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }

    public String getQtetotal() {
        return qtetotal;
    }

    public void setQtetotal(String qtetotal) {
        this.qtetotal = qtetotal;
    }

    public void addproduct(Product p) {
        if(products == null) {
            products = new ArrayList<>();
        }
        products.add(p);

    }


}
