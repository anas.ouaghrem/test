package com.vente.springjwt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.util.Collection;

@Entity
@Table(name="client")
public class Client extends User {


    private String adress;
    private String city;

    private String image;

    @OneToMany (mappedBy = "client",cascade = CascadeType.REMOVE)
    private Collection<Order> orders;




    public Client(String username, String email, String password, String adress, String city) {
        super(username, email, password);
        this.adress = adress;
        this.city = city;
    }
   public  Client(){}
    public String getAdress() {
        return adress;
    }

    public String getCity() {
        return city;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setCity(String city) {
        this.city = city;
    }


    @JsonIgnore
    public Collection<Order> getOrders() {
        return orders;
    }

    public void setOrders(Collection<Order> orders) {
        this.orders = orders;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
