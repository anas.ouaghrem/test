package com.vente.springjwt.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Collection;

@Entity
public class Panier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long idUser ;
    private String  date_de_cration;
    private Float total;

    @OneToMany(mappedBy = "panier",cascade = CascadeType.REMOVE)
    private Collection<Product> Products;
    @JsonIgnore
    public Collection<Product> getProducts() {
        return Products;
    }

    public void setProducts(Collection<Product> products) {
        Products = Products;
    }


    @OneToMany(mappedBy = "panier",cascade = CascadeType.REMOVE)
    private Collection<Immobilier> Immobiliers;
    @JsonIgnore
    public Collection<Immobilier> getImmobiliers() {
        return Immobiliers;
    }

    public void setImmobiliers(Collection<Immobilier> immobiliers) {
        Immobiliers = immobiliers;
    }




    @OneToMany(mappedBy = "panier",cascade = CascadeType.REMOVE)
    private Collection<Vehicule> Vehicules;
    @JsonIgnore
    public Collection<Vehicule> getVehicules() {
        return Vehicules;
    }

    public void setVehicules(Collection<Vehicule> vehicules) {
        Vehicules = vehicules;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getDate_de_cration() {
        return date_de_cration;
    }

    public void setDate_de_cration(String date_de_cration) {
        this.date_de_cration = date_de_cration;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }
}
