package com.vente.springjwt.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Collection;

@Entity
public class Commande {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
    private Long qtetotal;
    private String date_cmd;
    private String description_cmd ;
    private String duree_cmd;
    private String etat_cmd;


    @ManyToOne
    @JoinColumn(name = "Client_id")
    private Client client;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }



    @ManyToMany(mappedBy = "commandes",cascade = CascadeType.REMOVE)
    private Collection<Product> Products;
    @JsonIgnore
    public Collection<Product> getProducts() {
        return Products;
    }

    public void setProducts(Collection<Product> Products) {
        this.Products = Products;
    }

    @ManyToMany(mappedBy = "commandes",cascade = CascadeType.REMOVE)
    private Collection<Immobilier> Immobiliers;
    @JsonIgnore
    public Collection<Immobilier> getImmobiliers() {
        return Immobiliers;
    }

    public void setImmobiliers(Collection<Immobilier> immobiliers) {
        Immobiliers = immobiliers;
    }

    @ManyToMany(mappedBy = "commandes",cascade = CascadeType.REMOVE)
    private Collection<Vehicule> Vehicules;

    @JsonIgnore

    public Collection<Vehicule> getVehicules() {
        return Vehicules;
    }

    public void setVehicules(Collection<Vehicule> vehicules) {
        Vehicules = vehicules;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQtetotal() {
        return qtetotal;
    }

    public void setQtetotal(Long qtetotal) {
        this.qtetotal = qtetotal;
    }

    public String getDate_cmd() {
        return date_cmd;
    }

    public void setDate_cmd(String date_cmd) {
        this.date_cmd = date_cmd;
    }

    public String getDescription_cmd() {
        return description_cmd;
    }

    public void setDescription_cmd(String description_cmd) {
        this.description_cmd = description_cmd;
    }

    public String getDuree_cmd() {
        return duree_cmd;
    }

    public void setDuree_cmd(String duree_cmd) {
        this.duree_cmd = duree_cmd;
    }

    public String getEtat_cmd() {
        return etat_cmd;
    }

    public void setEtat_cmd(String etat_cmd) {
        this.etat_cmd = etat_cmd;
    }
}
