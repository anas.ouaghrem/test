package com.vente.springjwt.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Collection;

@Entity
public class Subcategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;


    @ManyToOne
    @JoinColumn(name="category_id")
    private Category category;



    @OneToMany (mappedBy = "subcategory",cascade = CascadeType.REMOVE)
    private Collection<Product> products;


    @OneToMany(mappedBy = "subcategory",cascade = CascadeType.REMOVE)
    private Collection<Vehicule> Vehicules;

    @JsonIgnore
    public Collection<Vehicule> getVehicules() {
        return Vehicules;
    }

    public void setVehicules(Collection<Vehicule> vehicules) {
        Vehicules = vehicules;
    }

    @OneToMany(mappedBy = "subcategory",cascade = CascadeType.REMOVE)
    private Collection<Immobilier> Immobiliers;
    @JsonIgnore
    public Collection<Immobilier> getImmobiliers() {
        return Immobiliers;
    }

    public void setImmobiliers(Collection<Immobilier> immobiliers) {
        Immobiliers = immobiliers;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }



    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

   @JsonIgnore
    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }
}
