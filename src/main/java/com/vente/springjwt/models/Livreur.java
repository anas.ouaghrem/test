package com.vente.springjwt.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Collection;

@Entity
public class Livreur  extends User {

    private String phone;
    private String available;
    private String refvehicle;

    @OneToMany(mappedBy = "livreur",cascade = CascadeType.REMOVE)
    private Collection<Livraison> livraisons;

    @JsonIgnore
    public Collection<Livraison> getLivraisons() {
        return livraisons;
    }

    public void setLivraisons(Collection<Livraison> livraisons) {
        this.livraisons = livraisons;
    }

    public Livreur(String username, String email, String password, String phone, String available, String refvehicle) {
        super(username, email, password);
        this.phone = phone;
        this.available = available;
        this.refvehicle = refvehicle;
    }

    public Livreur() {
    }

    public Livreur(String phone, String available, String refvehicle, Collection<Livraison> livraisons) {
        this.phone = phone;
        this.available = available;
        this.refvehicle = refvehicle;
        this.livraisons = livraisons;
    }



    public Livreur(String username, String email, String password, String phone, String available, String refvehicle, Collection<Livraison> livraisons) {
        super(username, email, password);
        this.phone = phone;
        this.available = available;
        this.refvehicle = refvehicle;
        this.livraisons = livraisons;
    }





    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getRefvehicle() {
        return refvehicle;
    }

    public void setRefvehicle(String refvehicle) {
        this.refvehicle = refvehicle;
    }
}



