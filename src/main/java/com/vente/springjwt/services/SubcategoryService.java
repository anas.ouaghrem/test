package com.vente.springjwt.services;


import com.vente.springjwt.models.Category;
import com.vente.springjwt.models.Subcategory;
import com.vente.springjwt.repository.CategoryRepository;
import com.vente.springjwt.repository.SubcategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubcategoryService {
    @Autowired
    SubcategoryRepository subcategoryRepository;
    @Autowired
    CategoryRepository categoryRepository;

    public List<Subcategory> getall(){
        return subcategoryRepository.findAll() ;
    }

    public Subcategory createSubcategoryService ( Subcategory s) {
        return subcategoryRepository.save(s);
    }

    public Subcategory createSubcategoryService ( Subcategory s , Long id_category){
            Category c = categoryRepository.findById(id_category).orElse(null);
            s.setCategory(c);
            return subcategoryRepository.save(s);
    }

    public ResponseEntity deleteSubcategoryService ( Long id) {
        subcategoryRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public Subcategory updateSubcategoryService ( Subcategory s, Long id){
        Subcategory s1 = subcategoryRepository.findById(id).orElse(null);
        if (s1 !=null){
            s.setId(id);
            return subcategoryRepository.saveAndFlush(s);
        }
        else{
            throw new RuntimeException("fail");
        }

    }
}
