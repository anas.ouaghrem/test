package com.vente.springjwt.services;


import com.vente.springjwt.models.Reclamation;
import com.vente.springjwt.models.User;
import com.vente.springjwt.repository.ReclamationRepository;
import com.vente.springjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReclamationService {
    @Autowired
    ReclamationRepository reclamationRepository;
    @Autowired
    UserRepository userRepository;

    public List<Reclamation> getall(){
        return reclamationRepository.findAll() ;    }


    public Reclamation creatReclamationService ( Reclamation r ,  Long id_user){
        User u = userRepository.findById(id_user).orElse(null);
        r.setUser(u);
        return reclamationRepository.save(r);
    }

    public ResponseEntity deleteReclamationService ( Long id) {
        reclamationRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public Reclamation updateReclamationService ( Reclamation r, Long id) {
        Reclamation r1 = reclamationRepository.findById(id).orElse(null);
        if (r1 != null) {
            r.setId(id);
            return reclamationRepository.saveAndFlush(r);
        } else {
            throw new RuntimeException("fail");
        }
    }



}
