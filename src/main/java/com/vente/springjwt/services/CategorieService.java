package com.vente.springjwt.services;



import com.vente.springjwt.models.Category;
import com.vente.springjwt.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategorieService {

    @Autowired
    CategoryRepository categorieRepository;

    public List<Category> getall(){
        return categorieRepository.findAll() ;    }


    public Category createCategorieService ( Category c){
        return categorieRepository.save(c);
    }


    public ResponseEntity deleteCategorieService ( Long id) {
        categorieRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }


    public Category updateCategorieService ( Category c,  Long id){
        Category c1 = categorieRepository.findById(id).orElse(null);
        if (c1 !=null){
            c.setId(id);
            return categorieRepository.saveAndFlush(c);
        }
        else{
            throw new RuntimeException("fail");
        }

    }



}
