package com.vente.springjwt.services;



import com.vente.springjwt.models.Livraison;
import com.vente.springjwt.repository.LivraisonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LivraisonService {
    @Autowired
    LivraisonRepository livraisonRepository;

    public List<Livraison> getall(){
        return livraisonRepository.findAll() ;    }

    public Livraison createLivraisonService ( Livraison l){
        return livraisonRepository.save(l);
    }

    public ResponseEntity deleteLivraisonService ( Long id) {
        livraisonRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public Livraison updateLivraisonService ( Livraison l,  Long id){
        Livraison l1 = livraisonRepository.findById(id).orElse(null);
        if (l1 !=null){
            l.setId(id);
            return livraisonRepository.saveAndFlush(l);
        }
        else{
            throw new RuntimeException("fail");
        }

    }



}
