package com.vente.springjwt.services;



import com.vente.springjwt.models.*;
import com.vente.springjwt.repository.ImmobilierRepository;
import com.vente.springjwt.repository.ProviderRepository;
import com.vente.springjwt.repository.SubcategoryRepository;
import com.vente.springjwt.utils.StorageService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;


import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
@CrossOrigin("*")
public class ImmobilierService {

    @Autowired
    ImmobilierRepository immobilierRepository;
    @Autowired
    ProviderRepository providerRepository;
    @Autowired
    SubcategoryRepository subcategoryRepository;
    @Autowired
    private StorageService storage;
    private final Path rootLocation = Paths.get("upload");




    public List<Immobilier> getall(){
        return immobilierRepository.findAll() ;
    }

    public Immobilier getOneImmobilierService( Long id){
        return immobilierRepository.findById(id).orElse(null);
    }

    public ResponseEntity deleteImmobilierService (Long id) {
        immobilierRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public Immobilier createImmobilierService (Immobilier p){
        return immobilierRepository.save(p);
    }


    public Immobilier updateImmobilierService (Immobilier i, Long id){
        Immobilier i1 = immobilierRepository.findById(id).orElse(null);
        if (i1 != null) {
            i.setId(id);
            return immobilierRepository.saveAndFlush(i);
        } else {
            throw new RuntimeException("fail");
        }

    }


    public ResponseEntity<ResponseMessage> uploadfilesService (MultipartFile[]files, Immobilier i, Long
            id_Subcategory, Long id_provider){
        String message = "";
        try {
            ArrayList<String> fileNames = new ArrayList<>();
            Arrays.asList(files).stream().forEach(file -> {
                try {
                    String fileName = Integer.toString(new Random().nextInt(1000000));
                    String ext = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf('.'),
                            file.getOriginalFilename().length());
                    String name = file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf('.'));
                    String original = name + fileName + ext;
                    Files.copy(file.getInputStream(), this.rootLocation.resolve(original));
                    fileNames.add(original);
                    i.setImage(fileNames);
                } catch (Exception e) {
                    throw new RuntimeException("fail file problem backend");
                }
            });

            Provider pr = providerRepository.findById(id_provider).orElse(null);
            i.setProvider(pr);
            Subcategory sub = subcategoryRepository.findById(id_Subcategory).orElse(null);
            i.setSubcategory(sub);

            immobilierRepository.save(i);
            message = "Uploaded the file successufully" + fileNames;
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));

        } catch (Exception e) {
            message = "fail to upload";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }


    public Immobilier saveImmobilierService (MultipartFile file, Immobilier immobilier, Long id_subcategory, Long
            id_provider){
        try {
            String fileName = Integer.toString(new Random().nextInt(1000000));
            String ext = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf('.'),
                    file.getOriginalFilename().length());
            String name = file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf('.'));
            String original = name + fileName + ext;
            Files.copy(file.getInputStream(), this.rootLocation.resolve(original));
            Provider pr = providerRepository.findById(id_provider).orElse(null);
            immobilier.setProvider(pr);
            Subcategory sub = subcategoryRepository.findById(id_subcategory).orElse(null);
            immobilier.setSubcategory(sub);
            immobilier.setImage(original);
            return immobilierRepository.save(immobilier);

        } catch (Exception e) {
            throw new RuntimeException("fail file problem backend");
        }
    }

    public ResponseEntity<Resource> getFileService (String filename){
        Resource file = storage.loadFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachement;filename=\"" + file.getFilename() + "\"")
                .body(file);
    }
}
