package com.vente.springjwt.services;


import com.vente.springjwt.models.Comment;
import com.vente.springjwt.models.User;
import com.vente.springjwt.repository.CommentRepository;
import com.vente.springjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    UserRepository userRepository;

    public List<Comment> getall(){
        return commentRepository.findAll() ;    }

    public Comment creatCommentService ( Comment c ,  Long id_user){
        User u = userRepository.findById(id_user).orElse(null);
        c.setUser(u);
        return commentRepository.save(c);
    }
    public ResponseEntity deleteCommentService ( Long id) {
        commentRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }


    public Comment updateCommentService ( Comment c,  Long id){
        Comment c1 = commentRepository.findById(id).orElse(null);
        if (c1 !=null){
            c.setId(id);
            return commentRepository.saveAndFlush(c);
        }
        else{
            throw new RuntimeException("fail");
        }

    }
}
