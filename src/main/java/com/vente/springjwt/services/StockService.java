package com.vente.springjwt.services;


import com.vente.springjwt.models.Stock;
import com.vente.springjwt.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockService {

    @Autowired
    StockRepository stockRepository;

    public List<Stock> getall(){
        return stockRepository.findAll() ;
    }

    public Stock getOneStockService( Long id){
        return stockRepository.findById(id).orElse(null);
    }


    public Stock createStockService (Stock s){
        return stockRepository.save(s);
    }


    public ResponseEntity deleteStockService (Long id) {
        stockRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public Stock updateStockService ( Stock s, Long id){
        Stock s1 = stockRepository.findById(id).orElse(null);
        if (s1 !=null){
            s.setId(id);
            return stockRepository.saveAndFlush(s);
        }
        else{
            throw new RuntimeException("fail");
        }

    }




}
