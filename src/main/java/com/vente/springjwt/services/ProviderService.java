package com.vente.springjwt.services;


import com.vente.springjwt.models.Provider;
import com.vente.springjwt.models.User;
import com.vente.springjwt.repository.ProviderRepository;
import com.vente.springjwt.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;

@Service
public class ProviderService {
    @Autowired
    ProviderRepository providerRepository;

    @Autowired
    private StorageService storage;
    private final Path rootLocation = Paths.get("upload");

    public List<Provider> getall() {
        return providerRepository.findAll();
    }

    public Provider getOneProviderService(Long id) {
        return providerRepository.findById(id).orElse(null);
    }

    public Provider createProviderService(Provider f) {
        return providerRepository.save(f);
    }

    public ResponseEntity deleteProviderService(Long id) {
        providerRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public Provider updateProviderService(Provider f, Long id) {
        Provider f1 = providerRepository.findById(id).orElse(null);
        if (f1 != null) {
            f.setId(id);
            return providerRepository.saveAndFlush(f);
        } else {
            throw new RuntimeException("fail");
        }

    }


}




