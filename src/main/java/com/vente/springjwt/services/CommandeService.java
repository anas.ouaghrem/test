package com.vente.springjwt.services;



import com.vente.springjwt.models.Commande;
import com.vente.springjwt.repository.CommandeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommandeService {
    @Autowired
    CommandeRepository commandeRepository;

    public List<Commande> getall(){
        return commandeRepository.findAll() ;    }


    public Commande createCommandeService ( Commande c){
        return commandeRepository.save(c);
    }

    public ResponseEntity deleteCommandeService ( Long id) {
        commandeRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public Commande updateCommandeService ( Commande c, Long id){
        Commande c1 = commandeRepository.findById(id).orElse(null);
        if (c1 !=null){
            c.setId(id);
            return commandeRepository.saveAndFlush(c);
        }
        else{
            throw new RuntimeException("fail");
        }

    }



}
