package com.vente.springjwt.services;

import com.vente.springjwt.models.Panier;
import com.vente.springjwt.repository.PanierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PanierService {
    @Autowired
    PanierRepository panierRepository;

    public List<Panier> getall(){
        return panierRepository.findAll() ;    }

    public Panier createPanierService ( Panier p){
        return panierRepository.save(p);
    }

    public ResponseEntity deletePanierSerrvice ( Long id) {
        panierRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
    public Panier updatePanierSevice ( Panier p,  Long id){
        Panier p1 = panierRepository.findById(id).orElse(null);
        if (p1 !=null){
            p.setId(id);
            return panierRepository.saveAndFlush(p);
        }
        else{
            throw new RuntimeException("fail");
        }


    }
}
