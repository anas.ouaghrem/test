package com.vente.springjwt.services;


import com.vente.springjwt.models.Client;
import com.vente.springjwt.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {
    @Autowired
    ClientRepository clientRepository;


    public List<Client> getall(){
        return clientRepository.findAll() ;    }

    public Client createClientService ( Client c){
        return clientRepository.save(c);
    }

    public ResponseEntity deleteClientService ( Long id) {
        clientRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public Client updateClientService ( Client c,  Long id){
        Client c1 = clientRepository.findById(id).orElse(null);
        if (c1 !=null){
            c.setId(id);
            return clientRepository.saveAndFlush(c);
        }
        else{
            throw new RuntimeException("fail");
        }

    }

}
