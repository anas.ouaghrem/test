package com.vente.springjwt.services;


import com.vente.springjwt.models.*;
import com.vente.springjwt.repository.ReclamationRepository;
import com.vente.springjwt.repository.UserRepository;
import com.vente.springjwt.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    ReclamationRepository reclamationRepository;
    @Autowired
    private StorageService storage;
    private final Path rootLocation = Paths.get("upload");

    public List<User> getall() {
        return userRepository.findAll();
    }

    public User getOneUserService(Long id) {
        return userRepository.findById(id).orElse(null);
    }


    public User createUserService(User u) {
        return userRepository.save(u);
    }

    public ResponseEntity deleteUserService(Long id) {
        userRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public User updateUserService(User u, Long id) {
        User u1 = userRepository.findById(id).orElse(null);
        if (u1 != null) {
            u.setId(id);
            return userRepository.saveAndFlush(u);
        } else {
            throw new RuntimeException("fail");
        }
    }


//    public ResponseEntity<ResponseMessage> uploadfilesService (MultipartFile[]files, User u){
//        String message = "";
//        try {
//            ArrayList<String> fileNames = new ArrayList<>();
//            Arrays.asList(files).stream().forEach(file -> {
//                try {
//                    String fileName = Integer.toString(new Random().nextInt(1000000));
//                    String ext = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf('.'),
//                            file.getOriginalFilename().length());
//                    String name = file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf('.'));
//                    String original = name + fileName + ext;
//                    Files.copy(file.getInputStream(), this.rootLocation.resolve(original));
//                    fileNames.add(original);
//                    u.setImage(fileNames);
//                } catch (Exception e) {
//                    throw new RuntimeException("fail file problem backend");
//                }
//            });
//            userRepository.save(u);
//            message = "Uploaded the file successufully" + fileNames;
//            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
//
//        } catch (Exception e) {
//            message = "fail to upload";
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
//        }
//    }
//


//    public User saveuserService(MultipartFile file, User user) {
//        try {
//            String fileName = Integer.toString(new Random().nextInt(1000000));
//            String ext = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf('.'),
//                    file.getOriginalFilename().length());
//            String name = file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf('.'));
//            String original = name + fileName + ext;
//            Files.copy(file.getInputStream(), this.rootLocation.resolve(original));
//            user.setImage(original);
//            return userRepository.save(user);
//
//        } catch (Exception e) {
//            throw new RuntimeException("fail file problem backend");
//        }
//    }
//

    public ResponseEntity<Resource> getFileService(String filename) {
        Resource file = storage.loadFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachement;filename=\"" + file.getFilename() + "\"")
                .body(file);
    }







    public ResponseEntity<ResponseMessage> uploadfilesService (MultipartFile[]files, User u){
        String message = "";
        try {
            ArrayList<String> fileNames = new ArrayList<>();
            Arrays.asList(files).stream().forEach(file -> {
                try {
                    String fileName = Integer.toString(new Random().nextInt(1000000));
                    String ext = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf('.'),
                            file.getOriginalFilename().length());
                    String name = file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf('.'));
                    String original = name + fileName + ext;
                    Files.copy(file.getInputStream(), this.rootLocation.resolve(original));
                    fileNames.add(original);
                    u.setImage(fileNames);
                } catch (Exception e) {
                    throw new RuntimeException("fail file problem backend");
                }
            });


            userRepository.save(u);
            message = "Uploaded the file successufully" + fileNames;
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));

        } catch (Exception e) {
            message = "fail to upload";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }


}
