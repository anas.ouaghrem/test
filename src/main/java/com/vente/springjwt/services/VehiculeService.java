package com.vente.springjwt.services;



import com.vente.springjwt.models.Provider;
import com.vente.springjwt.models.ResponseMessage;
import com.vente.springjwt.models.Subcategory;
import com.vente.springjwt.models.Vehicule;
import com.vente.springjwt.repository.ProviderRepository;
import com.vente.springjwt.repository.SubcategoryRepository;
import com.vente.springjwt.repository.VehiculeRepository;
import com.vente.springjwt.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
@CrossOrigin("*")
public class VehiculeService {

    @Autowired
    VehiculeRepository vehiculeRepository;
    @Autowired
    ProviderRepository providerRepository;
    @Autowired
    SubcategoryRepository subcategoryRepository;
    @Autowired
    private StorageService storage;
    private final Path rootLocation = Paths.get("upload");



    public List<Vehicule> getall(){
        return vehiculeRepository.findAll() ;
    }

    public Vehicule getOneVehiculeService( Long id){
        return vehiculeRepository.findById(id).orElse(null);
    }

    public ResponseEntity deleteVehiculeService (Long id) {
        vehiculeRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public Vehicule createVehiculeService (Vehicule v){
        return vehiculeRepository.save(v);
    }


    public Vehicule updateVehiculeService (Vehicule v, Long id){
        Vehicule v1 = vehiculeRepository.findById(id).orElse(null);
        if (v1 != null) {
            v.setId(id);
            return vehiculeRepository.saveAndFlush(v);
        } else {
            throw new RuntimeException("fail");
        }

    }


    public ResponseEntity<ResponseMessage> uploadfilesService (MultipartFile[]files, Vehicule v, Long
            id_subcategory, Long id_provider){
        String message = "";
        try {
            ArrayList<String> fileNames = new ArrayList<>();
            Arrays.asList(files).stream().forEach(file -> {
                try {
                    String fileName = Integer.toString(new Random().nextInt(1000000));
                    String ext = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf('.'),
                            file.getOriginalFilename().length());
                    String name = file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf('.'));
                    String original = name + fileName + ext;
                    Files.copy(file.getInputStream(), this.rootLocation.resolve(original));
                    fileNames.add(original);
                    v.setImage(fileNames);
                } catch (Exception e) {
                    throw new RuntimeException("fail file problem backend");
                }
            });

            Provider pr = providerRepository.findById(id_provider).orElse(null);
            v.setProvider(pr);
            Subcategory sub = subcategoryRepository.findById(id_subcategory).orElse(null);
            v.setSubcategory(sub);

            vehiculeRepository.save(v);
            message = "Uploaded the file successufully" + fileNames;
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));

        } catch (Exception e) {
            message = "fail to upload";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }


    public Vehicule saveVehiculeService (MultipartFile file, Vehicule vehicule, Long id_subcategory, Long
            id_provider){
        try {
            String fileName = Integer.toString(new Random().nextInt(1000000));
            String ext = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf('.'),
                    file.getOriginalFilename().length());
            String name = file.getOriginalFilename().substring(0, file.getOriginalFilename().indexOf('.'));
            String original = name + fileName + ext;
            Files.copy(file.getInputStream(), this.rootLocation.resolve(original));
            Provider pr = providerRepository.findById(id_provider).orElse(null);
            vehicule.setProvider(pr);
            Subcategory sub = subcategoryRepository.findById(id_subcategory).orElse(null);
            vehicule.setSubcategory(sub);
            vehicule.setImage(original);
            return vehiculeRepository.save(vehicule);

        } catch (Exception e) {
            throw new RuntimeException("fail file problem backend");
        }
    }


    public ResponseEntity<Resource> getFileService (String filename){
        Resource file = storage.loadFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachement;filename=\"" + file.getFilename() + "\"")
                .body(file);
    }
}


