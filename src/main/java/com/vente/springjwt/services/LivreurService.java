package com.vente.springjwt.services;

import com.vente.springjwt.models.Livreur;
import com.vente.springjwt.models.Livreur;
import com.vente.springjwt.models.Livreur;
import com.vente.springjwt.models.Livreur;
import com.vente.springjwt.repository.LivreurRepository;

import com.vente.springjwt.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


@Service
public class LivreurService {

    @Autowired
    LivreurRepository livreurRepository;

    @Autowired
    private StorageService storage;
    private final Path rootLocation = Paths.get("upload");


    public List<Livreur> getall() {
        return livreurRepository.findAll();
    }

    public Livreur getOneLivreurService(Long id) {
        return livreurRepository.findById(id).orElse(null);
    }

    public Livreur createLivreurService(Livreur f) {
        return livreurRepository.save(f);
    }

    public ResponseEntity deleteLivreurService(Long id) {
        livreurRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    public Livreur updateLivreurService(Livreur l, Long id) {
        Livreur l1 = livreurRepository.findById(id).orElse(null);
        if (l1 != null) {
            l.setId(id);
            return livreurRepository.saveAndFlush(l);
        } else {
            throw new RuntimeException("fail");
        }

    }




}
