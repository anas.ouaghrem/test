package com.vente.springjwt.repository;


import com.vente.springjwt.models.Immobilier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImmobilierRepository extends JpaRepository<Immobilier,Long> {
}
