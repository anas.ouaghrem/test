package com.vente.springjwt.repository;


import com.vente.springjwt.models.Panier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PanierRepository extends JpaRepository<Panier,Long> {
}
