package com.vente.springjwt.repository;



import com.vente.springjwt.models.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock,Long> {
}
