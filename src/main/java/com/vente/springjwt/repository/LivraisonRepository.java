package com.vente.springjwt.repository;


import com.vente.springjwt.models.Livraison;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LivraisonRepository extends JpaRepository<Livraison,Long> {
}
