package com.vente.springjwt.repository;


import com.vente.springjwt.models.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {
    @Query("select m from Product m where m.name like :x")
    List<Product> chercher(@Param("x") String x);

    @Query("select m from Product m ")
    Page<Product> findbypage(Pageable pageable);


}































