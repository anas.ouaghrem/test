package com.vente.springjwt.repository;



import com.vente.springjwt.models.Subcategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubcategoryRepository extends JpaRepository<Subcategory,Long> {


    @Query("SELECT s FROM Subcategory s WHERE s.category.id = :id_categorie")
    public List<Subcategory> findByCategoryId(@Param("id_categorie") Long id_categorie);
}
