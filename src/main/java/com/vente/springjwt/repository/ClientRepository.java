package com.vente.springjwt.repository;



import com.vente.springjwt.models.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client,Long> {
    @Query("select m from Client m where m.username= :name")
    Client findByClientname(@Param("name") String name);

    Client findByEmail(String email);
}
