package com.vente.springjwt.repository;



import com.vente.springjwt.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {
    @Query("select  o from Order o where o.client.username= :name")
    List<Order> findByClient(@Param("name") String name);


}
